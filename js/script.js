document.addEventListener("DOMContentLoaded", function () {
    console.log("DOM fully loaded and parsed");



    // let button = document.querySelector(".button");
    // let menu = document.querySelector(".menu");
    // button.style.color = "red";

    //
    // button.addEventListener("click", function(){
    //     if(menu.style.height === '400px'){
    //
    //         menu.style.borderBottomRightRadius = "0px";
    //         menu.style.borderBottomLeftRadius = "0px";
    //         menu.style.height = "0";
    //
    //     }else{
    //
    //         menu.style.borderBottomRightRadius = "125px";
    //         menu.style.borderBottomLeftRadius = "125px";
    //         menu.style.height = "400px";
    //     }
    //
    //
    // });

    let hamburger = document.querySelector('#hamburger-icon');
    let mainHeader = document.querySelector('.main-header');

    hamburger.addEventListener('click',function(e){
        e.preventDefault();
        if(hamburger.classList.contains('active')){


            hamburger.classList.remove('active');

            mainHeader.classList.add('close');
            mainHeader.classList.remove('open');

        }else{


            hamburger.classList.add('active');

            mainHeader.classList.add('open');
            mainHeader.classList.remove('close');
        }

    });




    let glide1 = new Glide('.glide', {
        type: 'carousel'
    }).mount();

    let currentScrollPos = 0;
    let offset = 150;
    let isHidden = false;
    let nav = document.querySelector('.main-header');

    window.addEventListener('scroll', function (e) {
        if (this.scrollY > (currentScrollPos + offset)) {
            if (!isHidden) {
                nav.classList.add('hide');
                if(mainHeader.classList.contains('open')){
                    hamburger.classList.remove('active');
                    mainHeader.classList.add('close');
                    mainHeader.classList.remove('open');

                }
                isHidden = true;
            }

            currentScrollPos = this.scrollY;
        }
        else if (this.scrollY < (currentScrollPos - offset)) {
            if (isHidden) {

                nav.classList.remove('hide');
                isHidden = false;
            }

            currentScrollPos = this.scrollY;
        }
    });

    let boutonRetour = document.querySelector('.retour-top');
    // boutonRetour.addEventListener('click', scrollToTop);

    //
    boutonRetour.addEventListener('click', function(e){
        e.preventDefault();
        document.querySelector('#top').scrollIntoView({
            behavior: 'smooth'
        });
    });
});